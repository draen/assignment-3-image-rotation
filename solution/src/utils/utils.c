#include "utils.h"

void print_message(const char* const message) {
    fprintf(stdout, "%s", message);
}

void println_message(const char* const message) {
    print_message(message);
    print_message("\n");
}


void print_error(const char* const message) {
    fprintf(stderr, "%s", message);
}

void println_error(const char* const message) {
    print_error(message);
    print_error("\n");
}

