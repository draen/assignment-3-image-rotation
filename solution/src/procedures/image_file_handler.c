#include "image_file_handler.h"


enum procedure_status read_image(const char* const file_name, read_image_func read_image_func, struct image* const result) {
    //open the file
    FILE* file = NULL;
    enum open_status open_status = file_open(&file, file_name, "r");
    if (open_status!=OPEN_OK) return PROCEDURE_ERR;

    //read image from the file (format specifics handled by read_image_func)
    enum read_status read_status = read_image_func(file, result);
    if (read_status!=READ_OK) {
        println_error(read_error_messages[read_status]);
        return PROCEDURE_ERR;
    }

    //close the file
    enum close_status close_status = file_close(file);
    if (close_status!=CLOSE_OK) return PROCEDURE_ERR;

    return PROCEDURE_OK;
}

enum procedure_status write_image(const char* const file_name, write_image_func write_image_func, const struct image image) {
    //open the file
    FILE* file = NULL;
    enum open_status open_status = file_open(&file, file_name, "w");
    if (open_status!=OPEN_OK) return PROCEDURE_ERR;

    //write iamge to the file (format specifics handled by write_image_func)
    enum write_status write_status = write_image_func(file, image);
    if (write_status!=WRITE_OK) {
        println_error(write_error_messages[write_status]);
        return PROCEDURE_ERR;
    }

    //close the file
    enum close_status close_status = file_close(file);
    if (close_status!=CLOSE_OK) return PROCEDURE_ERR;

    return PROCEDURE_OK;
}
