#include <stdio.h>

#include "procedures/image_file_handler.h"
#include "formats_bmp/bmp_reader.h"
#include "formats_bmp/bmp_writer.h"
#include "image_operations/rotate.h"
#include "img/image.h"
#include "procedures/procedure_codes.h"
#include "utils/utils.h"

int main( int argc, char** argv ) {
    println_message("Image transformer started...");
    if (argc != 3) {
        println_error("Incorrect args provided; usage: `image-transformer <original image file> <transformed image file>`");
        return 0;
    }
    const char* const input_file_name = argv[1];
    const char* const output_file_name = argv[2];

    struct image initial_image = {0};
    enum procedure_status is_read_success = read_image(input_file_name, bmp_read_image, &initial_image);
    //if read wasn't successful exit (error message already displayed by read_image function)
    if (is_read_success != PROCEDURE_OK){
        free_image(initial_image);
        println_message("Image transformer encountered an error and finished working");
        return 0;
    } 

    const struct image new_image = rotate(initial_image);
    free_image(initial_image); //don't need initial_image anymore

    enum procedure_status is_write_success = write_image(output_file_name, bmp_write_image, new_image);
    //if write wasn't successful exit (error message already displayed by write_image function)
    if (is_write_success != PROCEDURE_OK){ 
        free_image(new_image);
        println_message("Image transformer encountered an error and finished working");
        return 0;
    }

    free_image(new_image);

    println_message("Image transformer successfully finished working");
    return 0;
}
