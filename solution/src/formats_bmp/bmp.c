#include "bmp.h"

uint8_t bmp_padding_of(size_t width) {
    uint8_t res = width*sizeof(struct pixel) % 4;
    if (res) return 4-res;  //if res is not 0 then padding is `4-res`, otherwise it's `0`
    return 0;
}
