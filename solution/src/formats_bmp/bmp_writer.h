#ifndef BMP_WRITER_H
#define BMP_WRITER_H

#include <stdio.h>

#include "../formats_common/io_codes.h"
#include "../img/image.h"
#include "bmp.h"

//function for writing an image to .bmp file
enum write_status bmp_write_image(FILE* const file, const struct image source);



#endif
