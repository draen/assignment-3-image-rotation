#include "bmp_writer.h"

#define FORMAT_TYPE 0x4d42
#define OFFSET_TO_DATA sizeof(struct bmp_header)
#define INFO_HEADER_SIZE 40
#define PLANES_NUM 1
#define BIT_COUNT 24
#define COMPRESSION_TYPE 0
#define COLORS_NUM 0
#define H_RESOLUTION 2835
#define V_RESOLUTION 2835

static struct bmp_header HEADER_TEMPLATE = {
        .bfType = FORMAT_TYPE,              //signature
        .bfReserved = 0,                    //unused field
        .bOffBits = OFFSET_TO_DATA,         //offset before data (header size)
        .biSize = INFO_HEADER_SIZE,         //size of info header
        .biPlanes = PLANES_NUM,             //number of planes
        .biBitCount = BIT_COUNT,            //24 bits for 16M colors
        .biCompression = COMPRESSION_TYPE,  //compression type, 0 for no compression
        .biXPelsPerMeter = H_RESOLUTION,    //horizontal resolution
        .biYPelsPerMeter = V_RESOLUTION,    //vertical resolution
        .biClrUsed = COLORS_NUM,            //num of colors used
        .biClrImportant = 0                 //number of important colors, 0 for all
};

static struct bmp_header create_header(const struct image source) {
    struct bmp_header header = HEADER_TEMPLATE;
    header.biSizeImage = get_size(source);
    header.bfileSize = header.biSizeImage + sizeof(struct bmp_header);
    header.biWidth = source.width;
    header.biHeight = source.height;

    return header;
}

static enum write_status bmp_write_header(FILE* const file, const struct image source) {
    struct bmp_header header = create_header(source);
    size_t items_num = fwrite(&header, sizeof(struct bmp_header), 1, file);
    if (items_num != 1) return WRITE_ERR_HEADER;
    return WRITE_OK;
}

static enum write_status bmp_write_pixels(FILE* const file, const struct image source) {

    size_t width = source.width;
    uint8_t padding = bmp_padding_of(width);

    //just a zero for easier padding filling
    uint64_t zero = 0;

    //iterate through each row of the image
    for (size_t i = 0; i < source.height; i++) {
        size_t items_num = fwrite(source.pixels + width*i, sizeof(struct pixel), width, file);
        if (items_num != width) return WRITE_ERR_PIXELS;

        //fill padding
        size_t padding_bytes_num = fwrite(&zero, 1, padding, file);
        if (padding_bytes_num != padding) return WRITE_ERR_PADDING;
    }
    return WRITE_OK;
}

enum write_status bmp_write_image(FILE* const file, const struct image source) {
    enum write_status header_status = bmp_write_header(file, source);
    if (header_status!=WRITE_OK) return header_status;

    enum write_status image_status = bmp_write_pixels(file, source);
    return image_status;
}
