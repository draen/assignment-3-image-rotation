#include "bmp_reader.h"

static enum read_status bmp_read_header(FILE* const file, struct bmp_header* const result) {
    size_t items_num = fread(result, sizeof(struct bmp_header), 1, file);
    if (items_num!=1) return READ_INVALID_HEADER;
    return READ_OK;
}
static enum read_status bmp_read_pixels(FILE* const file, struct image* const result) {
    size_t width = result->width;
    uint8_t padding = bmp_padding_of(width);

    //iterate through each row of the image
    for (size_t i = 0; i < result->height; i++) {
        size_t items_num = fread(result->pixels + width*i, sizeof(struct pixel), width, file);
        if (items_num!=width) return READ_INVALID_SIGNATURE;

        //skip the padding at the end of the row
        int skip_res = fseek(file, padding, SEEK_CUR);
        if (skip_res!=0) return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum read_status bmp_read_image(FILE* const file, struct image* const result) {
    struct bmp_header header = {0};
    enum read_status header_status = bmp_read_header(file, &header);
    if (header_status != READ_OK) return header_status;

    *result = alloc_image(header.biWidth, header.biHeight);
    enum read_status pixels_status = bmp_read_pixels(file, result);
    return pixels_status;
}
