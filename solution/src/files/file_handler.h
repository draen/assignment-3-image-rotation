#ifndef FILE_HANDLER_H
#define FILE_HANDLER_H

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "../utils/utils.h"
#include "file_codes.h"

//function for opening a file (wrapper for fopen(...), displays error if it encounters one)
enum open_status file_open(FILE** const result_file, const char* const file_name, const char* const mode);

//function for closing a file (wrapper for fclose(...), displays error if it encounters one)
enum close_status file_close(FILE* const file);

#endif
