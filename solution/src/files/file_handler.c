#include "file_handler.h"


enum open_status file_open(FILE** const result_file, const char* const file_name, const char* const mode) {
    *result_file = fopen(file_name, mode);
    if (*result_file != NULL) return OPEN_OK;
    const char* const error_desc = strerror(errno);
    print_error("Error encountered when trying to open a file: ");
    println_error(error_desc);
    return OPEN_ERR;
}

enum close_status file_close(FILE* const file) {
    int res = fclose(file);
    if (res == 0) return CLOSE_OK; //if errno!=0 - error
    const char* const error_desc = strerror(errno);
    print_error("Error encountered when trying to close a file: ");
    println_error(error_desc);
    return CLOSE_ERR;
}
