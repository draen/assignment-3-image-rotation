#include "rotate.h"

struct image rotate(struct image const src) {
    struct image new = alloc_image(src.height, src.width);    

    for (size_t i = 0; i < src.height; i++) {
        for (size_t j = 0; j < src.width; j++) {
            set_pixel(&new, j, new.width-i-1, get_pixel(&src, i, j));
        }
    }
    return new;
}
