#ifndef ROTATE_H
#define ROTATE_H

#include "../img/image.h"

struct image rotate( struct image const src);

#endif
