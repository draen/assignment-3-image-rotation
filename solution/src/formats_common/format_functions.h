#ifndef FORMAT_FUNCTIONS_H
#define FORMAT_FUNCTIONS_H

#include <stdio.h>

#include "../img/image.h"
#include  "io_codes.h"

typedef  enum read_status (read_image_func)(FILE* const, struct image* const);
typedef enum write_status (write_image_func)(FILE* const, const struct image);


#endif
