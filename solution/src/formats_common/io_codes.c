#include "io_codes.h"

const char* const read_error_messages[] = {
[READ_ERR] = "Couldn't read image from file",
[READ_INVALID_HEADER] = "Invalid file header encountered",
[READ_INVALID_SIGNATURE] = "Invalid signature encountered",
[READ_INVALID_BITS] = "Invalid bits encountered"
};

const char* const write_error_messages[] = {
[WRITE_ERR] = "Write error encountered",
[WRITE_ERR_HEADER] = "Couldn't write header to file",
[WRITE_ERR_PIXELS] = "Couldn't write pixels to file",
[WRITE_ERR_PADDING] = "Couldn't write padding to file"
};
