#include "image.h"


struct image alloc_image(size_t width, size_t height) {
    struct pixel* pixels = malloc(sizeof(struct pixel)*width*height);
    struct image res = (struct image){.width = width, .height = height, .pixels = pixels};
    return res;
}

void free_image(struct image image) {
    free(image.pixels);
}

struct pixel get_pixel(const struct image* const src, size_t row, size_t col) {
    return src->pixels[ src->width * row + col ];
}

void set_pixel(struct image* const dest, size_t row, size_t col, struct pixel pixel){
    dest->pixels[ dest->width * row + col ] = pixel;
}

size_t get_size(const struct image image) {
    return sizeof(struct pixel) * image.width * image.height;
}
